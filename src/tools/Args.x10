package tools;

import x10.util.Map;
import x10.util.HashMap;

class Args {
	
	static val NUMBER_TYPE = 0n;
	static val STRING_TYPE = 1n;
	static val BOOLEAN_TYPE = 2n;
	static val FLOAT_TYPE = 3n;
	
	static val DEFAULT_ERROR = "<exec> -x <X_CELLS> -y <Y_CELLS> -outfile <OUTFILE_PREFIX> -s <SCENARIO_NUMBER> -l <USE_LOCAL_TIME_STEPPING> "
		+ "\n\nSCENARIO_NUMBER: \n\t0 -> PoolDrop\n\t1 -> RadialDamBreak\n\t2 -> LinearShoreWave"
		+ "\n\nUSE_LOCAL_TIME_STEPPING: \n\tfalse -> Global Time Stepping\n\ttrue -> Local Time Stepping";
	
	static struct Builder {
		val params : Map[String, Int];
		
		val description : Cell[String];
		
		def this() {
			this.params = new HashMap[String, Int]();
			this.description = Cell.make[String]("");
		}
		
		def param(name:String, typ:Int) {
			params.put(name, typ);
			if (typ > 3n || typ < 0n) {
				throw new IllegalArgumentException("Illicit value " + typ + ". Value must be either 0 = int, 1 = String,  2 = Boolean or 3 = Float.");
			}
			return this;
		}
		
		def description(description:String) {
			this.description() = description;
			return this;
		}
		
		def build() {
			if (description() == null) {
				throw new NullPointerException("Description must not be null");
			} else {
				return new Args(description(), params);
			}
		}	
	}
	
	val numberParams : Map[String, int];
	val stringParams : Map[String, String];
	val booleanParams : Map[String, Boolean];
	val floatParams : Map[String, Float];
	
	val allowedParams : Map[String, Int];
	
	
	def this(description:String, params:Map[String, Int]) {
		this.numberParams = new HashMap[String, int]();
		this.stringParams = new HashMap[String, String]();
		this.booleanParams = new HashMap[String, Boolean]();
		this.floatParams = new HashMap[String, Float]();
		
		this.allowedParams = params;
	}
	
	def read(args:Rail[String]) {
		assert args.size % 2 == 0 : "Arguments must be even <name> <value>";
		var key : String = null;
		for (word in args) {
			if (key == null) {
				key = word;
			} else {
				val value = word;
				file(key, value);
				key = null;
			}
		}
	}
	
	private def file(key:String, value: String) {
		assert allowedParams.containsKey(key) : "Argument " + key + " not allowed.";
		switch(allowedParams.get(key)) {
		case NUMBER_TYPE:
			numberParams.put(key, int.parse(value));
			break;
		case STRING_TYPE:
			stringParams.put(key, value);
			break;
		case BOOLEAN_TYPE:
			booleanParams.put(key, Boolean.parse(value));
			break;
		case FLOAT_TYPE:
			floatParams.put(key, Float.parse(value));
			break;
		}
	}
	
	public def toString() {
		return "" 
		+ "Passed Arguments:"
		+ "\n\tint:"
		+ "\n\t" + Debug.mtos(numberParams)
		+ "\n\tString:"
		+ "\n\t" + Debug.mtos(stringParams)
		+ "\n\tBoolean:"
		+ "\n\t" + Debug.mtos(booleanParams)
		+ "\n\tFloat:"
		+ "\n\t" + Debug.mtos(floatParams);
	}
	
}
