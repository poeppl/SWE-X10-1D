package tools;

import x10.regionarray.*;

import x10.util.Set;
import x10.util.Map;

public class Debug {
	
	public static val out = new Debug();

    public static def pwp(a:Any) {
        Console.OUT.println("Place " + ((here.id < 10) ? "0" : "") +  here.id + "\t" + a);
    }
    
    public static def  atos[A](arr:Rail[A], num:int) {
    	var x : String = "[ ";
    	var cnt : int = 0n;
    	for (element in arr) {
    		x += element.toString();
    		x += " ";
    		if (cnt == num) {
    			break;
    		} else {
    			cnt++;
    		}
    	}
    	x += "]";
    	return x;
    }
    
    public static def atos[A](arr:Array[A]{rank!=2}) {
    	var x : String = "[";
    	for (point in arr) {
    		x += arr(point).toString();
    		x += " ";
    	}
    	x += "]";
    	return x;
    }
    
    public static def atos2d[A](arr:Array[A]{rank==2}) {
    	var res : String = "";
    	val minx = arr.region.minPoint()(1);
    	val maxx = arr.region.maxPoint()(1);
    	val miny = arr.region.minPoint()(0);
    	val maxy = arr.region.maxPoint()(0);
    	for (y in miny..maxy) {
    		res += "\n| ";
    		for (x in minx..maxx) {
    			res += "("+y+","+x+") = " + arr(y,x);
    			res += " ";
    		}
    		res += "|";
    	}
    	
    	for ([y,x] in arr.region) {
    		res += "("+y+","+x+") = " + arr(y,x);
    		res += " ";
    	}
    	
    	for (p in arr.region) {
    		res += ""+ p + " = " + arr(p);
    		res += " ";
    	}
    	return res;
    }
    
    public static def mtos(map:Map[String, String]) {
    	var res : String = "{\n";
    	for (key in map.keySet()) {
    		val v = map.get(key);
    		res += "\t" + key.toString() + " -> " + map.get(key).toString() + "\n";
    	}
    	res += "}";
    	return res;
    }
    
    public static def mtos(map:Map[String, int]) {
    	var res : String = "{\n";
    	for (key in map.keySet()) {
    		val v = map.get(key);
    		res += "\t" + key.toString() + " -> " + map.get(key).toString() + "\n";
    	}
    	res += "}";
    	return res;
    }
    
    public static def mtos(map:Map[String, Boolean]) {
    	var res : String = "{\n";
    	for (key in map.keySet()) {
    		val v = map.get(key);
    		res += "\t" + key.toString() + " -> " + map.get(key).toString() + "\n";
    	}
    	res += "}";
    	return res;
    }
    
    public static def mtos(map:Map[String, Float]) {
    	var res : String = "{\n";
    	for (key in map.keySet()) {
    		val v = map.get(key);
    		res += "\t" + key.toString() + " -> " + map.get(key).toString() + "\n";
    	}
    	res += "}";
    	return res;
    }
    
    public operator this << (a:Any) {
    	Debug.pwp(a);
    	return this;
    }
}
