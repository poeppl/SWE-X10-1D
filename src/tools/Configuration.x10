package tools;

import x10.compiler.Inline;

public struct Configuration {
	
	private static val SIMULATION_SIZE_KEY = "-cells";
	private static val OUTFILE_KEY = "-out";
	private static val END_KEY = "-end";
	private static val DELTA_KEY = "-delta";
	private static val PATCH_SIZE_KEY = "-ps";

	private static val SIMULATION_SIZE_DEFAULT_VALUE = 400n;
	private static val PATCH_SIZE_DEFAULT_VALUE = 200n;
	private static val OUTFILE_DEFAULT_VALUE = "out/test";
	private static val END_DEFAULT_VALUE = 30.f;
	private static val DELTA_DEFAULT_VALUE = 0.5f;	
		
	private static val DEFAULT_ARGS_BUILDER = new Args.Builder()
		.param(SIMULATION_SIZE_KEY, Args.NUMBER_TYPE)
		.param(OUTFILE_KEY, Args.STRING_TYPE)
		.param(END_KEY, Args.FLOAT_TYPE)
		.param(DELTA_KEY, Args.FLOAT_TYPE) 
		.param(PATCH_SIZE_KEY, Args.NUMBER_TYPE)
		.description(Args.DEFAULT_ERROR);
	
	public val simulationSize : int;
	public val patchSize : int;
	public val maxTime : Float;
	public val outputDelta : Float;
	public val filePattern : String;
	
	private def this(args:Args) {
		simulationSize = getOr(args, SIMULATION_SIZE_KEY, SIMULATION_SIZE_DEFAULT_VALUE);
		filePattern = getOr(args, OUTFILE_KEY, OUTFILE_DEFAULT_VALUE);
		maxTime = getOr(args, END_KEY, END_DEFAULT_VALUE);
		outputDelta = getOr(args, DELTA_KEY, DELTA_DEFAULT_VALUE);
		patchSize = getPatchSize(args, simulationSize);
	}
	
	private def getOr[T](args:Args, key:String,  defaultVal:T) : T {
		if (defaultVal.typeName().equals("x10.lang.Float")) {
			return (args.floatParams.containsKey(key)) ? args.floatParams.get(key) as T : defaultVal;
		} else if (defaultVal.typeName().equals("x10.lang.Int")) {
			return (args.numberParams.containsKey(key)) ? args.numberParams.get(key) as T : defaultVal;
		} else if (defaultVal.typeName().equals("x10.lang.String")) {
			return (args.stringParams.containsKey(key)) ? args.stringParams.get(key) as T : defaultVal;
		} else if (defaultVal.typeName().equals("x10.lang.Boolean")) {
			return (args.booleanParams.containsKey(key)) ? args.booleanParams.get(key) as T : defaultVal;
		} 
		throw new Exception("Encountered illegal type for " + defaultVal + ": " + defaultVal.typeName());
	}
	
	private def getPatchSize(args:Args, simulationSize:int) {
		val readPatchSize = getOr(args, PATCH_SIZE_KEY, PATCH_SIZE_DEFAULT_VALUE);
		return Math.min(readPatchSize, simulationSize);
	}
	
	public static def read(args:Rail[String]) {
		val argReader = DEFAULT_ARGS_BUILDER.build();
		argReader.read(args);
		return new Configuration(argReader);
	}
}