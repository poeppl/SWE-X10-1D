package blocks;

import scenario.*;
import writer.*;
import solver.*;

public class SWEBlock(minOffset:Long, maxOffset:Long) {
	
	private val waterHeight : Rail[Float]{size == blockSize};
	private val momentum : Rail[Float]{size == blockSize};
	private val bathymetry : Rail[Float]{size == blockSize};
	
	private val waterHeightNetUpdates : Rail[Float]{size == blockSize};
	private val momentumNetUpdates : Rail[Float]{size == blockSize};
	
	private var maxWaveSpeed : Float;
	
	val blockSize = maxOffset - minOffset + 1 + 2;
	
	private val cellSize : Float;
	private val inverseCellSize : Float;
	private val writer : VtkWriter;
	private val solver : Solver;
	
	/**
	 * Creates an SWE Block. Designated initializer. Creates the data structure for the part of the simulation specified with the offsets.
	 * The size of the array is set as follows: number of simulated cells + 1 field for boundary or ghost layer on each side.
	 * 
	 * @param minOffset The smallest index of the simulated cells.
	 * @param maxOffset The biggest index of the simulated cells.
	 */
	public def this(minOffset:Long, maxOffset:Long, cellSize:Float, numberOfCells:Long) {
		property(minOffset, maxOffset);
		this.cellSize = cellSize;
		this.inverseCellSize = 1 / cellSize;
		this.waterHeight = new Rail[Float](maxOffset - minOffset + 1 + 2);
		this.momentum = new Rail[Float](maxOffset - minOffset + 1 + 2);
		this.bathymetry = new Rail[Float](maxOffset - minOffset + 1 + 2);
		this.waterHeightNetUpdates = new Rail[Float](maxOffset - minOffset + 1 + 2);
		this.momentumNetUpdates = new Rail[Float](maxOffset - minOffset + 1 + 2);
		this.maxWaveSpeed = 0.0f;
		this.writer = new VtkWriter("output", bathymetry, momentum, waterHeight, minOffset, maxOffset, cellSize, numberOfCells);
		this.solver = new Solver();
	}
	
	public def writeCurrentState() {
		writer.writeTimestep(0.f);
	}
	
	public def computeNetUpdates() {
		var maxWaveSpeed : Float = 0.f;
		for (i in 1..(blockSize-1)) {
			val result = solver.computeNetUpdates(waterHeight(i-1), waterHeight(i), momentum(i-1), momentum(i), bathymetry(i-1), bathymetry(i));
			waterHeightNetUpdates(i-1) += inverseCellSize * result.hUpdateLeft;
			waterHeightNetUpdates(i) += inverseCellSize * result.hUpdateRight;
			momentumNetUpdates(i-1) += inverseCellSize * result.huUpdateLeft;
			momentumNetUpdates(i) += inverseCellSize * result.huUpdateRight;
			maxWaveSpeed = Math.max(maxWaveSpeed, result.maxWaveSpeed);
		}
		return computeMaximumTimestep(maxWaveSpeed);
	}
	
	public def applyUpdates(timeDelta:Float) {
		for (i in 1..(blockSize-2)) {
			waterHeight(i) -= timeDelta * waterHeightNetUpdates(i);
			momentum(i) -= timeDelta * momentumNetUpdates(i);
			if (waterHeight(i) <= solver.DEFAULT_DRY_TOLERANCE) {
				momentum(i) = 0.0f;
			}
			if (waterHeight(i) < solver.DEFAULT_ZERO_TOLERANCE) {
				waterHeight(i) = 0.0f;
			}
		}
		waterHeightNetUpdates.clear();
		momentumNetUpdates.clear();
		
	}
	
	private def computeMaximumTimestep(maxWaveSpeed:Float) {
		if (maxWaveSpeed > solver.DEFAULT_ZERO_TOLERANCE) {
			return 0.4f * (cellSize/maxWaveSpeed);
		} else {
			return Float.MAX_VALUE;
		}
	}
	
	public def initializeScenario(scenario:SWEScenario) {
		for (i in minOffset..maxOffset) {
			val posInScene = (i-0.5f) * cellSize;
			val posInArray = i-minOffset+1;
			waterHeight(posInArray) = scenario.getWaterHeight(posInScene);
			momentum(posInArray) = scenario.getMomentum(posInScene);
			bathymetry(posInArray) = scenario.getBathymetry(posInScene);
		}
	}
	
	public def getLeftCopyLayer() = new SWECell(waterHeight(1), momentum(1), bathymetry(1));
	public def getRightCopyLayer() = new SWECell(waterHeight(blockSize-2), momentum(blockSize-2), bathymetry(blockSize-2));
	
	public def setLeftGhostLayer(cell:SWECell) {
		waterHeight(0) = cell.waterHeight;
		momentum(0) = cell.momentum;
		bathymetry(0) = cell.bathymetry;
	}
	
	public def setRightGhostLayer(cell:SWECell) {
		waterHeight(blockSize-1) = cell.waterHeight;
		momentum(blockSize-1) = cell.momentum;
		bathymetry(blockSize-1) = cell.bathymetry;
	}
}
