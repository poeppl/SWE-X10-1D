package blocks;

public class SWECell {
	public val waterHeight : Float;
	public val momentum : Float;
	public val bathymetry : Float;
	
	public def this(waterHeight:Float, momentum:Float, bathymetry:Float) {
		this.waterHeight = waterHeight;
		this.momentum = momentum;
		this.bathymetry = bathymetry;
	}
	
	public def toString() {
		return "[SWECell: h: " + waterHeight + ", m:" + momentum + ", b:" + bathymetry + "]";
	}
}