package simulation;

import tools.Configuration;
import tasklib.*;
import scenario.*;
import blocks.*;

public class SimulationDriver {
	public val config : Configuration;
	
	public def this(config:Configuration) {
		this.config = config;
		
	}
	
	
	public def simulate(scenario:SWEScenario) {
		
		//Setup simulation
		val simulatedSize = scenario.getTotalSimulationSize();
		val simulationSize = config.simulationSize;
		val cellSize = simulatedSize / simulationSize;
		val patchSize = config.patchSize;
		val tasks = new Rail[Task](Math.ceil(simulationSize/patchSize + 2) as Long);
		val leftBoundaryTask = new BoundaryTask("leftBoundary");
		val rightBoundaryTask = new BoundaryTask("rightBoundary");
		val simulationTasks = new Rail[GlobalRef[SimulationTask]](Math.ceil(simulationSize/patchSize) as Long);
		val simulationTaskNames = new Rail[String](simulationTasks.size);
		for (var idx : Long = 0; idx < simulationTasks.size; idx++) {
			val minSimCell = idx * patchSize;
			val maxSimCell = (idx+1) * patchSize -1;
			val taskName = "simTask-"+minSimCell+"-"+maxSimCell;
			val remoteTask = at (Place.FIRST_PLACE) {
				val block = new SWEBlock(minSimCell, maxSimCell, cellSize, simulationSize);
				block.initializeScenario(scenario);
				val simulationTask = new SimulationTask(block, "coordinator");
				return GlobalRef[SimulationTask](simulationTask);
			};
			tasks(idx+1) = remoteTask.evalAtHome((t:SimulationTask)=>new TaskRef(t.getSimulationTask() as TaskImpl));
			simulationTasks(idx) = remoteTask;
			simulationTaskNames(idx) = tasks(idx+1).name;
		}
		val coordinator = new CoordinationTask(simulationTaskNames, "coordinator", config.maxTime, config.outputDelta);
		val coordinationTask = coordinator.getCoordinationTask();
		
		//Setup Coordination
		tasks(0) = leftBoundaryTask.getBoundaryTask();
		tasks(0).connect(tasks(1));
		leftBoundaryTask.setNeighboringTask(simulationTaskNames(0));
		tasks(tasks.size-1) = rightBoundaryTask.getBoundaryTask();
		tasks(tasks.size-1).connect(tasks(tasks.size-2));
		rightBoundaryTask.setNeighboringTask(simulationTaskNames(simulationTaskNames.size-1));
		for (var idx : Long = 1; idx < tasks.size-1; idx++) {
			val taskRef = simulationTasks(idx-1);
			val leftNeighbor = tasks(idx-1);
			val rightNeighbor = tasks(idx+1);
			taskRef.evalAtHome((t:SimulationTask) => {
				t.setNeighbors(leftNeighbor.name, rightNeighbor.name);
				val task = t.getSimulationTask();
				task.connect(leftNeighbor);
				task.connect(rightNeighbor);
				task.connect(coordinationTask);
				coordinationTask.connect(task);
				return true;
			});
		}
		//Start Simulation
		coordinationTask.run();
		for (task in tasks) {
			task.run();
		}
	}
	
	
	
}