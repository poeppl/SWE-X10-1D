package simulation;

import blocks.*;
import tasklib.*;
import x10.array.DistArray;

public class CoordinationTask {
	public val taskName : String;
	public val simulationTasks:Rail[String];
	public val endTime : Float;
	public val timeDelta : Float;
	
	private var coordinationClosure:(Task)=>void;
	private var coordinationTask : Task;
	
	public def this(simulationTasks:Rail[String], taskName:String, endTime:Float, timeDelta:Float) {
		this.simulationTasks = simulationTasks;
		this.taskName = taskName;
		this.endTime = endTime;
		this.timeDelta = timeDelta;
	}
	
	private def initializeCoordinationClosure() {
		this.coordinationClosure = (t:Task) => {
			Console.OUT.println(coordinationTask.name + " started.");
			var time : Float = 0.0f;
			var nextWrite : Float = timeDelta;
			while (time < endTime) {
				var maxTimestep : Float = Float.POSITIVE_INFINITY;
				finish for (simulationTask in simulationTasks) async {
					val taskTimestep = t.read(simulationTask);
					atomic {
						maxTimestep = Math.min(taskTimestep as Float, maxTimestep);
					}
				}
				Console.OUT.println(time + " + " + maxTimestep);
				time += maxTimestep;
				if (time > nextWrite) {
					finish for (simulationTask in simulationTasks) async {
						t.write(simulationTask, 'w');
						t.write(simulationTask, maxTimestep);
					}
					nextWrite += timeDelta;
				} else {
					finish for (simulationTask in simulationTasks) async {
						t.write(simulationTask, maxTimestep);
					}
				}
			}
			for (simulationTask in simulationTasks) async {
				t.write(simulationTask, false);
			}
		};
	}
	
	public def getCoordinationTask() {
		if (coordinationTask == null) {
			initializeCoordinationClosure();
			coordinationTask = Task.make(taskName, coordinationClosure);
		}
		return coordinationTask;
	}
}