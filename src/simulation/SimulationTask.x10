package simulation;

import blocks.*;
import tasklib.*;

public class SimulationTask {
	public val taskName : String;
	private val block : SWEBlock;
	
	private var leftNeighbor : String;
	private var rightNeighbor : String;
	private var coordinator : String;
	
	private var simulationClosure:(Task)=>void;
	private var simulationTask : Task;
	
	public def this(block:SWEBlock, coordinator:String) {
		this.block = block;
		this.taskName = block.minOffset + "-" + block.maxOffset;
		this.simulationClosure = null;
		this.coordinator = coordinator;
	}
	
	private def initializeSimulationClosure() {
		this.simulationClosure = (t:Task) => {
			Console.OUT.println(simulationTask.name + " started.");
			while (true) {
				t.write(leftNeighbor, block.getLeftCopyLayer());
				t.write(rightNeighbor, block.getRightCopyLayer());
				val leftGhostLayerBlob = t.read(leftNeighbor);
				val rightGhostLayerBlob = t.read(rightNeighbor);
				if (!((leftGhostLayerBlob instanceof SWECell) && (rightGhostLayerBlob instanceof SWECell))) {
					t.write(leftNeighbor, false);
					t.write(rightNeighbor, false);
					break;
				}
				block.setLeftGhostLayer(leftGhostLayerBlob as SWECell);
				block.setRightGhostLayer(rightGhostLayerBlob as SWECell);
				val maxLocalTimestep = block.computeNetUpdates();
				t.write(coordinator, maxLocalTimestep);
				val tsData = t.read(coordinator);
				if (tsData instanceof Float) {
					block.applyUpdates(tsData as Float);
				} else if (tsData instanceof char && tsData as char == 'w') {
					val tsData2 = t.read(coordinator);
					block.applyUpdates(tsData2 as Float);
					block.writeCurrentState();
				} else {
					t.write(leftNeighbor, false);
					t.write(rightNeighbor, false);
					break;
				}
			}
			
		};
	}
	
	public def getSimulationTask() {
		if (simulationTask == null) {
			initializeSimulationClosure();
			this.simulationTask = Task.make(taskName, simulationClosure);
		}
		return simulationTask;
	}
	
	public def setNeighbors(leftNeighbor:String, rightNeighbor:String) {
		this.leftNeighbor = leftNeighbor;
		this.rightNeighbor = rightNeighbor;
	}
}