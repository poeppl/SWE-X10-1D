package simulation;

import tasklib.*;
import blocks.*;

public class BoundaryTask {
	
	public val taskName : String;
	
	private var neighboringTask : String;
	
	private var boundaryClosure : (t:Task) => void;
	private var boundaryTask : Task;
	
	public def this(taskName:String) {
		this.taskName = taskName;
		this.neighboringTask = null;
	}
	
	public def setNeighboringTask(newNeighbor:String) {
		this.neighboringTask = newNeighbor;
	}
	
	private def initializeBoundaryClosure() {
		boundaryClosure = (t:Task) =>  {
			Console.OUT.println(boundaryTask.name + " started.");
			var shouldStop : Boolean = false;
			do {
				val incomingData = t.read(neighboringTask);
				if (incomingData instanceof Boolean && !(incomingData as Boolean)) {
					shouldStop = true;
				} else if (incomingData instanceof SWECell) {
					val dataPoint = incomingData as SWECell;
					t.write(neighboringTask, new SWECell(dataPoint.waterHeight, -dataPoint.momentum, dataPoint.bathymetry));
				}
			} while (!shouldStop);
		};		
	}
	
	public def getBoundaryTask() {
		if (boundaryTask == null) {
			initializeBoundaryClosure();
			boundaryTask = Task.make(taskName, boundaryClosure);
		}
		return boundaryTask;
	}
	
}