package writer;


import x10.io.File;
import x10.io.Printer;

public class VtkWriter {
	
	private val cellSize : Float;
	private val filename : String;
	
	private val minIndex : Long;
	private val maxIndex : Long;
	
	private val totalNumberOfCells : Long;
	
	val bathymetry : Rail[Float];
	val momentum : Rail[Float];
	val waterHeight : Rail[Float];
	
	var timestep : Long;
	
	public def this(filename:String, bathymetry:Rail[Float], momentum:Rail[Float], waterHeight:Rail[Float], minIndex:Long, maxIndex:Long, cellSize:Float, totalNumberOfCells:Long) {
		this.filename = filename + "-" + minIndex + "-" + maxIndex;
		this.cellSize = cellSize;
		this.minIndex = minIndex;
		this.maxIndex = maxIndex;
		this.totalNumberOfCells = totalNumberOfCells;
		this.bathymetry = bathymetry;
		this.momentum = momentum;
		this.waterHeight = waterHeight;
		this.timestep = 0;
	}
		
	private def generateFilename() {
		return filename + "." + timestep++ + ".vtr";
	}
	 
	public def writeTimestep(time:Float) {
		val outputFile = new File(this.generateFilename());
		val fOut = outputFile.printer();
		
		//print VTK Header. 
		fOut.println("<?xml version=\"1.0\"?>");
		fOut.println("<VTKFile type=\"RectilinearGrid\">");
		fOut.println("<RectilinearGrid WholeExtent=\"" + minIndex + " " + (maxIndex+1) + " 0 0 0 0\">");
		fOut.println("<Piece Extent=\"" + minIndex + " " + (maxIndex+1) + " 0 0 0 0\">");
		
		//Define the Grid points.
		fOut.println("<Coordinates>");
		fOut.println("<DataArray type=\"Float32\" format=\"ascii\">");
		for (pt in minIndex..(maxIndex+1)) {
			fOut.println((pt * cellSize));
		}
		fOut.println("</DataArray>");
		fOut.println("<DataArray type=\"Float32\" format=\"ascii\">");
		fOut.println("0");
		fOut.println("</DataArray>");
		fOut.println("<DataArray type=\"Float32\" format=\"ascii\">");
		fOut.println("0");
		fOut.println("</DataArray>");
		
		fOut.println("</Coordinates>");

		//Write Cell Data
		fOut.println("<CellData>");

		//Cell data - Water Height
		fOut.println("<DataArray Name=\"water_height\" type=\"Float32\" format=\"ascii\">");
		for (idx in 1..(waterHeight.size-2)) {
			fOut.println(waterHeight(idx));
		}
		fOut.println("</DataArray>");

		//Cell data - Momentum
		fOut.println("<DataArray Name=\"momentum\" type=\"Float32\" format=\"ascii\">");
		for (idx in 1..(momentum.size-2)) {
			fOut.println(momentum(idx));
		}
		fOut.println("</DataArray>");
		
		//Cell data - Bathymetry
		fOut.println("<DataArray Name=\"bathymetry\" type=\"Float32\" format=\"ascii\">");
		for (idx in 1..(bathymetry.size-2)) {
			fOut.println(bathymetry(idx));
		}
		fOut.println("</DataArray>");
		
		//Print Footer
		fOut.println("</CellData>");
		fOut.println("</Piece>");
		fOut.println("</RectilinearGrid>");
		fOut.println("</VTKFile>");
		fOut.flush();
		fOut.close();
	}
	
	
	// public  def writeTimestep(h:Array[Float]{rect, rank == 2}, hu:Array[Float]{rect, rank == 2}, hv:Array[Float]{rect, rank == 2}, time:Float) {
	// 	val outputFile = new File(this.generateFilename());
	// 	val fOut = outputFile.printer();
	// 	
	// 	//print VTK Header. 
	// 	fOut.println("<?xml version=\"1.0\"?>");
	// 	fOut.println("<VTKFile type=\"StructuredGrid\">");
	// 			
	// 	val minx = region.minPoint()(1);
	// 	val miny = region.minPoint()(0);
	// 	val maxx = region.maxPoint()(1);
	// 	val maxy = region.maxPoint()(0);
	// 	
	// 	fOut.println("<StructuredGrid WholeExtent=\"" + minx + " " + (maxx+1) + " " + miny + " " + (maxy+1) + " 0 0\">");
	// 	fOut.println("<Piece Extent=\"" + minx + " " + (maxx+1) + " " + miny + " " + (maxy+1) + " 0 0\">");
	// 	
	// 	//Grid points.
	// 	fOut.println("<Points>");
	// 	fOut.println("<DataArray NumberOfComponents=\"3\" type=\"Float32\" format=\"ascii\">");
	// 	for (y in miny..(maxy+1)) {
	// 		for (x in minx..(maxx+1)) {
	// 			fOut.println((x*dx) + " " + (y*dy) + " 0");
	// 		}
	// 	}
	// 	fOut.println("</DataArray>");
	// 	fOut.println("</Points>");
	// 	
	// 	//Cell data - Water height.
	// 	fOut.println("<CellData>");
	// 	fOut.println("<DataArray Name=\"h\" type=\"Float32\" format=\"ascii\">");
	// 	for ([y,x] in region) {
	// 			fOut.println(h(y,x));
	// 	}
	// 	fOut.println("</DataArray>");		
	// 	
	// 	//Cell data - U momentum
	// 	fOut.println("<DataArray Name=\"hu\" type=\"Float32\" format=\"ascii\">");
	// 	for ([y,x] in region) {
	// 		fOut.println(hu(y,x));
	// 		
	// 	}
	// 	fOut.println("</DataArray>");
	// 	
	// 	//Cell data - V momentum
	// 	fOut.println("<DataArray Name=\"hv\" type=\"Float32\" format=\"ascii\">");
	// 	for (y in miny..maxy) {
	// 		for (x in minx..maxx) {
	// 			fOut.println(hv(y,x));
	// 		}
	// 	}
	// 	fOut.println("</DataArray>");
	// 	
	// 	//Cell data - Bathymetry
	// 	fOut.println("<DataArray Name=\"b\" type=\"Float32\" format=\"ascii\">");
	// 	for ([y,x] in region) {
	// 		fOut.println(bathymetry(y,x));
	// 	}
	// 	fOut.println("</DataArray>");
	// 	
	// 	//print VTK Footer
	// 	fOut.println("</CellData>");
	// 	fOut.println("</Piece>");
	// 	fOut.println("</StructuredGrid>");
	// 	fOut.println("</VTKFile>");
	// 	fOut.flush();
	// 	fOut.close();
	// 	timestep++;
	// }


}
