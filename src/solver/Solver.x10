package solver;

import x10.compiler.StackAllocate;

public class Solver {
	
	public static struct Result {
		public val hUpdateLeft : Float;
		public val hUpdateRight : Float;
		public val huUpdateLeft : Float;
		public val huUpdateRight : Float;
		public val maxWaveSpeed : Float;
		
		public def this(hUpdateLeft:Float, hUpdateRight:Float, huUpdateLeft:Float, huUpdateRight:Float, maxWaveSpeed:Float) {
			this.hUpdateLeft = hUpdateLeft;
			this.hUpdateRight = hUpdateRight;
			this.huUpdateLeft = huUpdateLeft;
			this.huUpdateRight = huUpdateRight;
			this.maxWaveSpeed = maxWaveSpeed;
		}
	}
	
	
	public static val H_UPDATE_LEFT_IDX   = 0;
	public static val H_UPDATE_RIGHT_IDX  = 1;
	public static val HU_UPDATE_LEFT_IDX  = 2;
	public static val HU_UPDATE_RIGHT_IDX = 3;
	public static val MAX_WAVE_SPEED_IDX  = 4;
	
	public static val FWAVE_FLOPS = 53;
	public static val HLLE_FLOPS = 135;
	public static val AUGRIE_FLOPS = 200;
	
	public val DEFAULT_ZERO_TOLERANCE = 0.0000001f;
	public val DEFAULT_GRAVITY = 9.81f;
	public val DEFAULT_DRY_TOLERANCE = 0.01f;
	
	
	/**
	 *   Both cells are dry. 
	 */
	public static val DryDry = 0;
	
	/**
	 *   Both cells are wet. 
	 */
	public static val WetWet = 1;
	
	/**
	 *   1st cell: wet, 2nd cell: dry. 1st cell lies higher than the 2nd one. 
	 */
	public static val WetDryInundation = 2;
	
	/** 
	 *   1st cell: wet, 2nd cell: dry. 1st cell lies lower than the 2nd one.
	 *   Momentum is not large enough to overcome the difference. 
	 */
	public static val WetDryWall = 3;
	
	/**  
	 *   1st cell: wet, 2nd cell: dry. 1st cell lies lower than the 2nd one.
	 *   Momentum is large enough to overcome the difference. 
	 */
	public static val WetDryWallInundation = 4;
	
	/**
	 *   1st cell: dry, 2nd cell: wet. 1st cell lies lower than the 2nd one.
	 */
	public static val DryWetInundation = 5;
	
	/**  
	 *   1st cell: dry, 2nd cell: wet. 1st cell lies higher than the 2nd one.
	 *   Momentum is not large enough to overcome the difference. 
	 */
	public static val DryWetWall = 6;
	
	/**  
	 *   1st cell: dry, 2nd cell: wet. 1st cell lies higher than the 2nd one.
	 *   Momentum is large enough to overcome the difference. 
	 */
	public static val DryWetWallInundation = 7;  
	
	/**
	 * Indicates at which water height a cell is considered to be dry. Wall boundary conditions are applied beneath that point.
	 */
	public val dryTolerance : Float;
	
	/**
	 * Gravity constant.
	 */
	public val g : Float;
	
	/**
	 * g/2. Used frequently, hence made available here.
	 */
	protected val halfG : Float;
	
	/**
	 * Sqrt(g). Used frequently, hence made available here.
	 */
	protected val sqrtG : Float;
	
	/**
	 * absolute val < zeroTolerance => static wave.
	 */
	public val zeroTolerance : Float;
	
	public def this() {
		this(0.01f, 9.81f, 0.0000001f);
	}
	
	/**
	 * Default constructor. 
	 */
	public def this(dryTolerance:Float, gravity:Float, zeroTolerance:Float) {
		this.dryTolerance = dryTolerance;
		this.g = gravity;
		this.zeroTolerance = zeroTolerance;
		this.sqrtG = Math.sqrt(g);
		this.halfG = 0.5f * g;
	}
	
	public def computeNetUpdates(i_hLeft:Float,  i_hRight:Float, 
			i_huLeft:Float, i_huRight:Float, 
			i_bLeft:Float,  i_bRight:Float) {
		//edge-local variables
		//! height on the left side of the edge (could change during execution).
		var hLeft : Float = i_hLeft;
		//! height on the right side of the edge (could change during execution).
		var hRight : Float = i_hRight;
		//! momentum on the left side of the edge (could change during execution).
		var huLeft : Float = i_huLeft;
		//! momentum on the right side of the edge (could change during execution).
		var huRight : Float = i_huRight;
		//! bathymetry on the left side of the edge (could change during execution).
		var bLeft : Float = i_bLeft;
		//! bathymetry on the right side of the edge (could change during execution).
		var bRight : Float = i_bRight;
		//! velocity on the left side of the edge (computed by determineWetDryState).
		var uLeft : Float = 0.0f;
		//! velocity on the right side of the edge (computed by determineWetDryState).
		var uRight : Float = 0.0f;		
		
		///////////////////////////
		// Determine WetDryState //
		///////////////////////////
		
		val wetDryState : Long;
		if (hLeft < dryTolerance && hRight < dryTolerance) { 
			//both cells are dry
			wetDryState = DryDry;
		} else if (hLeft < dryTolerance) { 
			// left cell dry, right cell wet
			uRight = huRight/hRight;
			
			//Set wall boundary conditions.
			//This is not correct in the case of inundation problems.
			hLeft = hRight;
			bLeft = bRight;
			huLeft = -huRight;
			uLeft = -uRight;
			wetDryState = DryWetWall;
		} else if (hRight < dryTolerance) { 
			//left cell wet, right cell dry
			uLeft = huLeft/hLeft;
			
			//Set wall boundary conditions.
			//This is not correct in the case of inundation problems.
			hRight = hLeft;
			bRight = bLeft;
			huRight = -huLeft;
			uLeft = -uRight;
			wetDryState = WetDryWall;
		} else { 
			//both cells wet
			uLeft = huLeft/hLeft;
			uRight = huRight/hRight;
			wetDryState = WetWet;
		}
		
		//zero updates and return in the case of dry cells
		if(wetDryState == DryDry) {
			return Solver.Result(0.0f,0.0f,0.0f,0.0f,0.0f);
		}
		
		/////////////////////////
		// Compute Wave Speeds //
		/////////////////////////
		
		
		//compute eigenvalues of the jacobian matrices in states Q_{i-1} and Q_{i}
		val characteristicSpeeds0 = uLeft - Math.sqrt(g*hLeft) as Float;
		val characteristicSpeeds1 = uRight + Math.sqrt(g*hRight) as Float;
		
		//compute "Roe speeds"
		val hRoe = 0.5f * (hRight + hLeft);
		var uRoe : Float = uLeft * (Math.sqrt(hLeft) as Float) + uRight * (Math.sqrt(hRight) as Float);
		uRoe /= Math.sqrt(hLeft) + Math.sqrt(hRight);
		
		val roeSpeeds0 = uRoe - Math.sqrt(g*hRoe) as Float;
		val roeSpeeds1 = uRoe + Math.sqrt(g*hRoe) as Float;
		
		//compute eindfeldt speeds
		val einfeldtSpeeds0 = Math.min(characteristicSpeeds0, roeSpeeds0);
		val einfeldtSpeeds1 = Math.max(characteristicSpeeds1, roeSpeeds1);
		
		//set wave speeds
		val waveSpeeds0 = einfeldtSpeeds0;
		val waveSpeeds1 = einfeldtSpeeds1;
		
		
		
		//! where to store the two f-waves
		val fWaves00 : Float;
		val fWaves01 : Float;
		val fWaves10 : Float;
		val fWaves11 : Float;
		
		///////////////////////////////////////////
		//compute the decomposition into f-waves //
		///////////////////////////////////////////
		
		assert(waveSpeeds0 < waveSpeeds1);
		
		val lambdaDif = waveSpeeds1 - waveSpeeds0;
		
		//assert: no division by zero
		assert(Math.abs(lambdaDif) > zeroTolerance);
		
		//compute the inverse matrix R^{-1}
		
		val oneDivLambdaDif = 1.0f /  lambdaDif;
		val Rinv00 = oneDivLambdaDif *  waveSpeeds1;
		val Rinv01 = -oneDivLambdaDif;
		val Rinv10 = oneDivLambdaDif * -waveSpeeds0;
		val Rinv11 = oneDivLambdaDif;
		
		//right hand side
		
		//calculate modified (bathymetry!) flux difference
		// f(Q_i) - f(Q_{i-1})
		var fDif0 : Float = huRight - huLeft;
		var fDif1 : Float = huRight * uRight + 0.5f * g * hRight * hRight
		-(huLeft  * uLeft  + 0.5f * g * hLeft  * hLeft);
		
		// \delta x \Psi[2]
		val psi = -g * 0.5f * (hRight + hLeft)*(bRight - bLeft);
		fDif1 -= psi;
		
		//solve linear equations
		val beta0 = Rinv00 * fDif0 + Rinv01 * fDif1;
		val beta1 = Rinv10 * fDif0 + Rinv11 * fDif1;
		
		//return f-waves
		fWaves00 = beta0;
		fWaves01 = beta0 * waveSpeeds0;
		
		fWaves10 = beta1;
		fWaves11 = beta1 * waveSpeeds1;
		
		
		/////////////////////////
		// Compute Net Updates //
		/////////////////////////
		
		var hLeftEnd : Float = 0.f;
		var hRightEnd : Float = 0.f;
		var huLeftEnd : Float = 0.f;
		var huRightEnd : Float = 0.f;
		val maxWaveSpeed : Float;
		
		//1st wave family
		if(waveSpeeds0 < -zeroTolerance) { //left going
			hLeftEnd += fWaves00;
			huLeftEnd += fWaves01;
		} else if (waveSpeeds0 > zeroTolerance) { //right going
			hRightEnd += fWaves00;
			huRightEnd += fWaves01;
		} else { //split waves
			hLeftEnd += .5f*fWaves00;
			huLeftEnd += .5f*fWaves01;
			hRightEnd += .5f*fWaves00;
			huRightEnd += .5f*fWaves01;
		}
		
		//2nd wave family
		if (waveSpeeds1 < -zeroTolerance) { //left going
			hLeftEnd += fWaves10;
			huLeftEnd += fWaves11;
		} else if (waveSpeeds1 > zeroTolerance) { //right going
			hRightEnd += fWaves10;
			huRightEnd += fWaves11;
		} else { //split waves
			hLeftEnd += 0.5f*fWaves10;
			huLeftEnd += 0.5f*fWaves11;
			hRightEnd += 0.5f*fWaves10;
			huRightEnd += 0.5f*fWaves11;
		}
		
		//compute maximum wave speed (-> CFL-condition)
		maxWaveSpeed = Math.max(Math.abs(waveSpeeds0), Math.abs(waveSpeeds1));
		return Solver.Result(hLeftEnd, hRightEnd, huLeftEnd, huRightEnd, maxWaveSpeed);
	}
}