package scenario;

public interface SWEScenario {
	public def getWaterHeight(position:Float) : Float;
	public def getMomentum(position:Float) : Float;
	public def getBathymetry(position:Float) : Float;
	public def getTotalSimulationSize() : Float;
}