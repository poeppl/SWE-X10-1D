package scenario;

public class DamBreakScenario implements SWEScenario {
	
	public def getWaterHeight(position:Float) {
		return (position > 450.0f && position < 550.f) ? 20.f : 10.f;
	}
	
	public def getBathymetry(position:Float) {
		return 0.0f;
	}
	
	public def getTotalSimulationSize() {
		return 1000.0f;
	}
	
	public def getMomentum(position:Float) {
		return 0.0f;
	}
	
	public def typeName() {
		return "scenario.DamBreakScenario";
	}
	
	public def equals(that:Any):Boolean {
		return that instanceof DamBreakScenario;
	}
}