package tasklib;

import x10.util.ArrayList;

public class DistChannel extends Channel {
  val buffer = new ArrayList[Any]();
  var remoteChannel : GlobalRef[DistChannel];
  var size : Long = 0;
  var seq : Long = 0;

  public def this(name:String, capacity:Long) {
    super(name, capacity);
  }

  public def write(elem:Any) {
    var localSeq : Long = 0;
    when (size < capacity) {
      size++;
      localSeq = seq++;
    }
    val seq = localSeq;
    val ref = remoteChannel;
    at (ref) async {
      val channel = ref();
      when (seq == channel.seq) {
        channel.seq++;
        channel.buffer.add(elem);
      }
    }
  }

  public def read():Any {
    var elem : Any = null;
    when (buffer.size() > 0) {
      elem = buffer.removeFirst();
    }
    val ref = remoteChannel;
    at (ref) async {
      val channel = ref();
      atomic {
        channel.size--;
      }
    }
    return elem;
  }

  def isReadable(n:Long) = buffer.size() >= n;

  def isWritable(n:Long) = capacity - size >= n;
}

