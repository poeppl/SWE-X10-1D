package tasklib;

public abstract class Channel(name:String, capacity:Long) {
  public static val DEFAULT_CHANNELSIZE = 1;

  def this(name:String, capacity:Long) {
    property(name, capacity);
  }

  public abstract def write(elem:Any):void;
  public abstract def read():Any;
  abstract def isReadable(n:Long):boolean;
  abstract def isWritable(n:Long):boolean;
  public def toString() = name;
}

