package tasklib;

import x10.util.HashMap;
import x10.util.concurrent.AtomicBoolean;

public class TaskImpl extends Task {
	
	static class Port {
		val channel : Channel;
		var nTokens : Long = 1;
		
		def this(channel:Channel) {
			this.channel = channel;
		}
	}
	val taskBody:(task:Task)=>void;
	val outPorts = new HashMap[String, Port]();
	val inPorts = new HashMap[String, Port]();
	val running = new AtomicBoolean(false);
	
	// TODO: Not serializable
	def this(name:String, taskBody:(task:Task)=>void) {
		super(name);
		this.taskBody = taskBody;
	}
	
	public def connect(task:Task, capacity:Long) {
		if (task instanceof TaskImpl) {
			val other = task as TaskImpl;
			val channel = new SharedChannel(this.name+"->"+other.name, capacity);
			/* Configure this task */
			this.outPorts.put(other.name, new Port(channel));
			/* Configure other task */
			other.inPorts.put(this.name, new Port(channel));
		} else {
			val other = task as TaskRef;
			val channel = new DistChannel(this.name+"->"+other.name, capacity);
			/* Configure this task */
			this.outPorts.put(other.name, new Port(channel));
			val channelRef = GlobalRef[DistChannel](channel);
			val thisName = this.name;
			val channelRef2 = at (other.ref) {
				val otherLocal = other.ref();
				val channel2 = new DistChannel(thisName+"->"+otherLocal.name, capacity);
				/* Configure other task */
				otherLocal.inPorts.put(thisName, new Port(channel2));
				channel2.remoteChannel = channelRef;
				GlobalRef[DistChannel](channel2)
			};
			channel.remoteChannel = channelRef2;
		}
		Console.OUT.println(this+" connected to "+task+" (Channel capacity: "+capacity+")");
	}
	
	public def run() {
		if (running.compareAndSet(false, true)) {
			async {
				taskBody(this);
				Console.OUT.println(this.name + " terminated.");
				running.set(false);
			}
		} else {
			Console.ERR.println(this+" is already running");
		}
	}
	
	public def write(name:String, elem:Any) {
		if (!outPorts.containsKey(name)) {
			Console.ERR.println(this+" has no write channel to "+name);
		} else {
			outPorts.get(name).channel.write(elem);
		}
	}
	
	public def read(name:String) {
		if (!inPorts.containsKey(name)) {
			Console.ERR.println(this+" has no read channel to "+name);
			return null;
		} else {
			return inPorts.get(name).channel.read();
		}
	}
	
	public def waitForFireCondition() {
		when (isReady());
	}
	
	def isReady() {
		/* Check input channels */
		for (entry in inPorts.entries()) {
			val port = entry.getValue();
			if (!port.channel.isReadable(port.nTokens)) {
				return false;
			}
		}
		/* Check output channels */
		for (entry in outPorts.entries()) {
			val port = entry.getValue();
			if (!port.channel.isWritable(port.nTokens)) {
				return false;
			}
		}
		return true;
	}
}

