package tasklib;

import x10.util.ArrayList;

public class SharedChannel extends Channel {
  val buffer = new ArrayList[Any]();

  public def this(name:String, capacity:Long) {
    super(name, capacity);
  }

  public def write(elem:Any) {
    when (buffer.size() < capacity) {
      buffer.add(elem);
    }
  }

  public def read():Any {
    when (buffer.size() > 0) {
      return buffer.removeFirst();
    }
  }

  def isReadable(n:Long) = buffer.size() >= n;

  def isWritable(n:Long) = capacity - buffer.size() >= n;
}

