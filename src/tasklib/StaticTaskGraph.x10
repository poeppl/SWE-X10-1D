package tasklib;

import x10.lang.annotations.MethodAnnotation;

// Must be used to mark the function that defines the task graph
// Only one taskgraph per file is allowed
// The taskgraph extractor expects exactly one taskgraph!
public interface StaticTaskGraph extends MethodAnnotation {
}
