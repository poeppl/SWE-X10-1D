package tasklib;

public abstract class Task(name:String) {
  def this(name:String) {
    property(name);
  }

  public static def make(name:String, taskBody:(task:Task)=>void) {
    val task = new TaskImpl(name, taskBody);
    Console.OUT.println(task+" created on "+here);
    return task;
  }

  public static def make(name:String, taskBody:(task:Task)=>void, place:Place) {
    if (place == here) {
      return make(name, taskBody);
    } else {
      return at (place) new TaskRef(make(name, taskBody));
    }
  }

  /* Can be called by remote tasks */
  public def connect(task:Task) {
    connect(task, Channel.DEFAULT_CHANNELSIZE);
  }
  public abstract def connect(task:Task, capacity:Long):void;
  public abstract def run():void;

  /* Cannot be called by remote task */
  public abstract def write(name:String, elem:Any):void;
  public abstract def read(name:String):Any;
  public abstract def waitForFireCondition():void;

  public def toString() = "Task "+name;
}

