package tasklib;



import x10.compiler.Inline;

public class ChannelUtilities {
  @Inline
  public static def writeToAllAsync(channels:Rail[Channel], message:Any) {
    for (channel in channels) async {
      channel.write(message);
    }
  }

  @Inline
  public static def waitAndConsumeMessage(channels:Rail[Channel]) {
    finish for (channel in channels) async {
      channel.read();
    }
  }
}

