package tasklib;

public class TaskRef extends Task {
  val ref:GlobalRef[TaskImpl];

  public def this(task:TaskImpl) {
    super(task.name);
    this.ref = GlobalRef[TaskImpl](task);
  }

  public def connect(task:Task, capacity:Long) {
    if (ref.home == here) {
      val r = ref as GlobalRef[TaskImpl]{self.home == here};
      r().connect(task, capacity);
    } else {
      if (task instanceof TaskImpl) {
        val taskRef = new TaskRef(task as TaskImpl);
        at (ref) {
          ref().connect(taskRef, capacity);
        }
      } else {
        at (ref) {
          ref().connect(task, capacity);
        }
      }
    }
  }

  public def run() {
    if (ref.home == here) {
      val r = ref as GlobalRef[TaskImpl]{self.home == here};
      r().run();
    } else {
      at (ref) {
        ref().run();
      }
    }
  }

  public def write(name:String, elem:Any) {
    throw new UnsupportedOperationException();
  }

  public def read(name:String):Any {
    throw new UnsupportedOperationException();
  }

  public def waitForFireCondition() {
    throw new UnsupportedOperationException();
  }
}

