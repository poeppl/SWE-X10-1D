package tasklib;

import tasklib.TaskAnnotation;

public interface CPUAnnotation(min:int, max:int) extends TaskAnnotation {
}
