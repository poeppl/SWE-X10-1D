package tasklib;

import x10.compiler.Inline;
import x10.compiler.Pure;

public class TaskUtilities {
  @Inline @Pure
  public static def wrapTask(task:TaskImpl) = new TaskRef(task);
  
  @Inline
  public static def asyncSendToAll(sender:Task, receivers:Rail[Task], message:Any) {
	  for (recv in receivers) async {
		  sender.write(recv.name, message);
	  }
  }

  @Inline
  public static def waitAndConsumeMessage(receiver:Task, senders:Rail[Task]) {
	  finish for (recv in senders) async {
		  receiver.read(recv.name);
	  }
  }
}

