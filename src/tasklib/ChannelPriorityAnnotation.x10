package tasklib;

import tasklib.ChannelAnnotation;

public interface ChannelPriorityAnnotation(priority:int) extends ChannelAnnotation {
}
