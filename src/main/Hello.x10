package main;

import blocks.*;
import writer.*;
import scenario.*;
import tools.*;
import simulation.*;

public class Hello {

    /**
     * The main method for the Hello class
     */
    public static def main(args:Rail[String]) {
    	val config = Configuration.read(args);
    	val driver = new SimulationDriver(config);
    	driver.simulate(new DamBreakScenario());
    	
    	/*val block1 = new SWEBlock(0, 100, 10.f);
    	block1.initializeScenario(new DamBreakScenario());
    	block1.writeCurrentState();
    
    	for (i in 0..500) {
    		val maxTime = block1.computeNetUpdates();
    		block1.applyUpdates(maxTime);
    		block1.writeCurrentState();
    	}
    	*/
    	Console.OUT.println("Simulation Done.");
    }
}